package calculator

import java.time.ZonedDateTime
import java.time.DayOfWeek._
import java.time.format.{DateTimeFormatter, DateTimeParseException}

object DueDateCalculator {

  type Date = ZonedDateTime

  val workingHourStart = 9
  val workingHourEnd = 17
  val workingHours: Int = workingHourEnd - workingHourStart
  val notWorkingHours: Int = 24 - workingHours

  def convertStringToDate(dateString: String): Date = {
    ZonedDateTime.parse(dateString)
  }

  def convertDateToString(date: Date): String = {
    val formatter = DateTimeFormatter.ISO_DATE_TIME
    date.format(formatter)
  }

  def nextWorkingDay(date: Date): Date = {
    date.getDayOfWeek match {
      case FRIDAY => date.plusDays(3)
      case SATURDAY => date.plusDays(2)
      case _ => date.plusDays(1)
    }
  }

  def addWorkingDays(date: Date, days: Int): Date = {
    var nextDate: Date = date
    for (i <- 1 to days) {
      nextDate = nextWorkingDay(nextDate)
    }
    nextDate
  }

  def addWorkingHours(date: Date, hours: Int): Date = {
    if (date.getHour + hours > workingHourEnd) {
      date.plusHours(hours + notWorkingHours)
    } else {
      date.plusHours(hours)
    }
  }

  @throws[DateTimeParseException]
  def calculateDueDate(submitDate: String, turnaroundTime: Int): String = {
    val date = convertStringToDate(submitDate)

    val plusDays = turnaroundTime / workingHours
    val plusHours = turnaroundTime % workingHours

    val datePlusDays = addWorkingDays(date, plusDays)
    val dueDate = addWorkingHours(datePlusDays, plusHours)

    convertDateToString(dueDate)
  }
}
