package test.acceptance

import java.time.{LocalDateTime, ZonedDateTime}

import org.scalatest.{FeatureSpec, GivenWhenThen, Matchers}
import calculator.DueDateCalculator._

class DueDateCalculatorSpec extends FeatureSpec with GivenWhenThen with Matchers {

  info("As a user")
  info("I want to calculate the due date of my work")

  feature("Due date calculation") {
    scenario("Right now I want to calculate the due date of my work where the turnaroundTime is 10 hours") {
      Given("The date of the calculation")
      val date = convertDateToString(ZonedDateTime.now)

      Given("The turnaroundTime")
      val turnaroundTime = 10

      When("I calculate my works due date")
      val dueDate = calculateDueDate(date, turnaroundTime)

      Then("I want the new date to be 10 working hours later")
      val newDateWithDays = addWorkingDays(convertStringToDate(date), 1)
      val expectedDueDate = addWorkingHours(newDateWithDays, 2)
      dueDate shouldBe convertDateToString(expectedDueDate)
    }
  }
}
