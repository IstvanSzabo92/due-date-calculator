package test.unit

import java.time.format.DateTimeParseException
import java.time.{DayOfWeek, ZonedDateTime}

import calculator.DueDateCalculator._
import org.scalatest.{FlatSpec, Matchers}

class DueDateCalculatorSpec extends FlatSpec with Matchers {

  "The calculateDayDate method" should "take the submit date and turnaround time as an input" in {
    val submitDate = "2019-08-09T10:00:00.000+02:00"
    val turnaroundTime = 8
    calculateDueDate(submitDate, turnaroundTime)
  }

  it should "return the date and time when the issue is to be resolved" in {
    val submitDate = "2019-08-09T10:10:00+02:00"
    val turnaroundTime = 8
    val dueDate = "2019-08-12T10:10:00+02:00"
    dueDate shouldBe calculateDueDate(submitDate, turnaroundTime)
  }

  it should "handle weekends and hour overflow" in {
    val fridayDate = "2019-08-09T16:00:00+02:00"
    val turnaroundTime = 21
    calculateDueDate(fridayDate, turnaroundTime) shouldBe "2019-08-14T13:00:00+02:00"
  }

  it should "return the same date when the turnaroundTime is 0" in {
    val date = "2019-08-09T16:00:00+02:00"
    val turnaroundTime = 0
    calculateDueDate(date, turnaroundTime) shouldBe date
  }

  it should "throw DateTimeParseException if the input is wrong" in {
    val wrongDate = "wrong"
    assertThrows[DateTimeParseException](
      calculateDueDate(wrongDate, 0)
    )
  }

  "The nextWorkingDay method" should "return Monday if the input was Friday" in {
    val fridayDate = convertStringToDate("2019-08-09T10:00:00+02:00")
    val newDate = nextWorkingDay(fridayDate)
    newDate.getDayOfWeek shouldBe DayOfWeek.MONDAY
  }

  it should "return Monday if the input was Saturday" in {
    val fridayDate = convertStringToDate("2019-08-10T10:00:00+02:00")
    val newDate = nextWorkingDay(fridayDate)
    newDate.getDayOfWeek shouldBe DayOfWeek.MONDAY
  }

  it should "return Wednesday if the input was Tuesday" in {
    val tuesdayDate = convertStringToDate("2019-08-06T10:00:00+02:00")
    val newDate = nextWorkingDay(tuesdayDate)
    newDate.getDayOfWeek shouldBe DayOfWeek.WEDNESDAY
  }

  "The addWorkingDays method" should "change the date to 2 days later" in {
    val date = convertStringToDate("2019-08-06T10:00:00+02:00")
    val expectedDate = convertStringToDate("2019-08-08T10:00:00+02:00")
    addWorkingDays(date, 2) shouldBe expectedDate
  }

  it should "change the date to 4 days later when input day is Friday and the number of days to add is 2" in {
    val date = convertStringToDate("2019-08-09T10:00:00+02:00")
    date.getDayOfWeek shouldBe DayOfWeek.FRIDAY
    val expectedDate = convertStringToDate("2019-08-13T10:00:00+02:00")
    addWorkingDays(date, 2) shouldBe expectedDate
  }

  "The addWorkingHours method" should "handle hour overflow" in {
    val date = convertStringToDate("2019-08-08T10:00:00+02:00")
    val hours = 10
    val expectedDate = convertStringToDate("2019-08-09T12:00:00+02:00")
    addWorkingHours(date, hours) shouldBe expectedDate
  }

  it should "add hour correctly without overflow" in {
    val date = convertStringToDate("2019-08-08T10:02:00+02:00")
    val hours = 3
    val expectedDate = convertStringToDate("2019-08-08T13:02:00+02:00")
    addWorkingHours(date, hours) shouldBe expectedDate
  }

  "convertStringToDate method" should "convert input to yyyy-MM-ddThh:mm:ssZ format" in {
    val date: String = "2019-08-08T10:00:00+02:00"
    val formattedDate: ZonedDateTime = ZonedDateTime.parse(date)
    convertStringToDate(date) shouldBe formattedDate
  }

  it should "throw DateTimeParseException if the input is wrong" in {
    val wrongDate = "wrong"
    assertThrows[DateTimeParseException](
      convertStringToDate(wrongDate)
    )
  }

  "convertDateToString method" should "convert input to yyyy-MM-ddThh:mm:ssZ format" in {
    val date: ZonedDateTime = ZonedDateTime.parse("2019-08-08T10:00:00+02:00")
    val expected: String = "2019-08-08T10:00:00+02:00"
    convertDateToString(date) shouldBe expected
  }
}
